package com.example.stopwatch

import android.os.AsyncTask
import android.os.SystemClock
import android.widget.TextView
import java.text.DecimalFormat

class AsyncTaskStopWatch(private var activity: MainActivity?, var bufferedTime: Int): AsyncTask<Void, Void, Void>(){

    var stopWatchTime: Int = bufferedTime
    var isStop: Boolean = false
    var isPause: Boolean=false

    override fun onPreExecute() {
        super.onPreExecute()
        activity?.buttonStart?.isEnabled=false
    }

    override fun doInBackground(vararg params: Void?):Void? {

        var tekst: String = ""
        var minutes: Int
        var seconds: Int
        var millis: Int
        isStop= false
        isPause=false
        var pocetak: Long
        var kraj: Long
        pocetak=SystemClock.uptimeMillis()-bufferedTime
        var decFormat = DecimalFormat("00")


        while (!isStop && !isPause){
            minutes= stopWatchTime/60000
            seconds = (stopWatchTime - minutes*60000)/1000
            millis=(stopWatchTime - minutes*60000)%1000
            tekst=decFormat.format(minutes)+":"+decFormat.format(seconds)+":"+
                    decFormat.format(millis).substring(0,2)
            activity?.textVievShowTime?.setText(tekst)
            print(tekst)
            //Thread.sleep(1-(SystemClock.uptimeMillis()-pocetak))
            //pocetak=SystemClock.uptimeMillis()
            stopWatchTime= SystemClock.uptimeMillis().toInt()-pocetak.toInt()
        }
        return null
    }

    override fun onPostExecute(result: Void?) {
        super.onPostExecute(result)
        activity?.buttonStart?.isEnabled=true
    }

    override fun onCancelled() {
        super.onCancelled()
        isStop= true
        stopWatchTime=0

    }

    fun stop():Int{
        isStop= true
        stopWatchTime=0
        return stopWatchTime
    }

    fun pause():Int{
        isPause= true
        return stopWatchTime
    }

}