package com.example.stopwatch

import android.os.AsyncTask
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Chronometer
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    lateinit var buttonStart: Button
    lateinit var buttonStop : Button
    lateinit var buttonPause: Button
    lateinit var textVievShowTime : TextView

    lateinit var task: AsyncTaskStopWatch

    var MillisecTime: Long = 0L
    var startTime: Long = 0

    var buffedTime: Int= 0
    //var MillisecTime: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        buttonStart = findViewById(R.id.buttonStart)
        buttonStop= findViewById(R.id.buttonStop)
        buttonPause= findViewById(R.id.buttonPause)
        textVievShowTime = findViewById(R.id.textViewShowTime)

        buttonStart.setOnClickListener(){
            this.task = AsyncTaskStopWatch(this, buffedTime)
            task.execute()
        }

        buttonStop.setOnClickListener {
            //task.cancel(true)
           buffedTime=task?.stop()
        }

        buttonPause.setOnClickListener {
            buffedTime= task?.pause()

        }

    }



}
